import { Component, OnInit } from '@angular/core';
import { Mail } from '../models/mail';
import { MailService } from '../services/mail-service';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.css']
})
export class MailComponent implements OnInit {
  mail: Mail = new Mail();
  action: string;
  constructor(private sharedService: SharedService ,private mailService: MailService,  private router: Router, public alert: AlertService) { }

  ngOnInit() {
  }
  submit(form: NgForm) {
   this.mailService.sendMail(this.mail).subscribe(data =>
  
   console.log(data)
  );
   
   ;
  
}  
}
