import { Component, OnInit } from '@angular/core';

import { Client } from '../models/client';
import { ClientService } from '../services/client-service';
import { Router } from '@angular/router';
import { SharedService } from '../services/shared.service';
import { UtilisateurService } from '../services/utilisateur-service';
import { AuthentificationService } from '../services/authentification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
user:any;
login:String="";
password:String="";

  constructor( private authService: AuthentificationService,private utilisateurService :UtilisateurService, private router: Router, public sharedService: SharedService)  { }

  ngOnInit() {

  }

mode: number = 0;

onLogin(user) {
 
  this.authService.login(user).subscribe(
    (data) => {
      

      let jwt = data.body['jwt'];
      let role= data.body['role'];
      //let role =data.body
     
      this.sharedService.setToken(jwt);
      this.sharedService.setRole(role);
     this.router.navigateByUrl('/accueil');
      
    },
    err => {
    
      this.mode = 1;
    })

   
    
   
}
  

}
