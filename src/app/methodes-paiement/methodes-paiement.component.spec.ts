import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MethodesPaiementComponent } from './methodes-paiement.component';

describe('MethodesPaiementComponent', () => {
  let component: MethodesPaiementComponent;
  let fixture: ComponentFixture<MethodesPaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MethodesPaiementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodesPaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
