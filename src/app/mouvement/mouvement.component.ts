import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { CompteService } from '../services/compte-service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { MouvementService } from '../services/mouvement.service';
import { Compte } from '../models/compte';
import { Mouvement } from '../models/mouvement';

@Component({
  selector: 'app-mouvement',
  templateUrl: './mouvement.component.html',
  styleUrls: ['./mouvement.component.css']
})
export class MouvementComponent implements OnInit {

  constructor(private sharedService: SharedService ,private mouvementService: MouvementService,private compteService: CompteService, private route: ActivatedRoute , private router: Router, private alert: AlertService) 
  { }
  compteId:any=null;
  compte:Compte;
  mouvementList: Array<Mouvement> = [];
  ngOnInit() {
    this.route.params.subscribe(params =>
      this.compteId = params['id']
     
    );
  
    if (this.sharedService.getToken() == null) {
      this.router.navigateByUrl('/login');
    }
     this.router.routeReuseStrategy.shouldReuseRoute = function () {
       return false;
     };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    });
    
      this.mouvementService.getAllMouvements().subscribe(
        (data) => {
       
        data.forEach(obj => {
        Object.entries(obj).forEach(([key, value]) => {
          if (key=='compte'){
              this.compte=value;
              if(this.compte.codeCompte==this.compteId){
                this.mouvementList.push(obj);
              }
          }
           
        });
    
    });
     
        }
      )
    }
    
   
   

}
