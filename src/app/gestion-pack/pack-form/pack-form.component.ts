import { Component, OnInit } from '@angular/core';
import { Pack } from 'src/app/models/pack';
import { PackService } from 'src/app/services/pack-service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-pack-form',
  templateUrl: './pack-form.component.html',
  styleUrls: ['./pack-form.component.css']
})
export class PackFormComponent implements OnInit {

  pack: Pack = new Pack();
    packId: any;
    action: string;
    isAjouter:Boolean=false;
    constructor(private packService: PackService, private route: ActivatedRoute, private router: Router, public alert: AlertService) { }
  
    ngOnInit() {
     
      this.route.params.subscribe(params =>
        this.packId = params['pack_id']
      );
      this.route.data.subscribe(data =>
        this.action = (data.action == "update") ? "Modifier" : "Ajouter" 
        
      )
      this.packService.getPackById(this.packId).subscribe(
        (data: Pack) => this.pack = data
        
      );
      this.isAjouter= (this.action=="Ajouter");
    }
    submit(form: NgForm) {
  
      if (this.action == "Ajouter")
        { this.isAjouter=true;
         
          this.save();}
      else if (this.action == "Modifier")
        this.update(this.packId);
    }
  
    save() {
      
      this.packService.createPack(this.pack)
        .subscribe(data => {
          this.alert.setAlert("success","Pack ajouté !","d-block play")
          this.router.navigateByUrl('/gestionPack')
  
        }, error => {
          this.alert.setAlert("danger","Erreur ! ajout non éffectué !","d-block play")
          this.router.navigateByUrl('/gestionPack')
        });
    }
    update(id: any) {
      this.packService.updatePack(id, this.pack)
        .subscribe(
          data => {
           
            this.pack = data as Pack;
            this.alert.setAlert("success","Modification éffectuée !","d-block play")
            this.router.navigateByUrl('/gestionPack');
  
          },
          error => {
            this.alert.setAlert("danger","Erreur ! modification non éffectuée !","d-block play")
            this.router.navigateByUrl('/gestionPack');
          })
    }
  }
