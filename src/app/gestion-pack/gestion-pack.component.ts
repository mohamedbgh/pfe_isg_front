import { Component, OnInit, Input } from '@angular/core';
import { Produit } from '../models/produit';
import { SharedService } from '../services/shared.service';
import { PackService } from '../services/pack-service';
import { Router, NavigationEnd } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { Pack } from '../models/pack';

@Component({
  selector: 'app-gestion-pack',
  templateUrl: './gestion-pack.component.html',
  styleUrls: ['./gestion-pack.component.css']
})
export class GestionPackComponent implements OnInit {
@Input() produit : Produit;
  constructor(private sharedService:SharedService ,private packService: PackService,  private router: Router, private alert: AlertService) { }
  type: string;
  message: string;
  classCss: string;
  eventFire: boolean = true;
  ngOnInit() {if (this.sharedService.getToken() == null) {
    this.router.navigateByUrl('/login');
  }
   this.router.routeReuseStrategy.shouldReuseRoute = function () {
     return false;

   };

  this.router.events.subscribe((evt) => {
    if (evt instanceof NavigationEnd) {
      this.router.navigated = false;
      window.scrollTo(0, 0);
    }
  });
  this.packService.getAllPacks().subscribe(
    (data) => {
     
      this.packsList = data;
 
    }
  )

  this.message = this.alert.getMessage();
  this.type = this.alert.getType();
  if (this.message && this.type) {
    this.classCss = this.alert.getClassCss();
  }
}
packsList: Array<Pack> = [];

ngOnDestroy() {
  if (this.eventFire) {
    this.alert.setAlert(null, null, "d-none stop");
  }
}

remove(id: number) {
  
   this.packService.deletePack(id).subscribe(
     (data) => {
      this.alert.setAlert("success", "Suppression éffectuée !", "d-block play");
       this.eventFire = false;
       this.router.navigateByUrl('/gestionPack');
     },
     (err) => {
      this.alert.setAlert("danger", "Erreur !", "d-block play");
      this.eventFire = false;
      this.router.navigateByUrl('/gestionPack');
     }
   );
}

 

}
