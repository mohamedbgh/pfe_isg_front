import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { AccueilComponent } from './accueil/accueil.component';
import { GestionClientComponent } from './gestion-client/gestion-client.component';
import { ClientFormComponent } from './gestion-client/client-form/client-form.component';
import { GestionProduitComponent } from './gestion-produit/gestion-produit.component';
import { ProduitFormComponent } from './gestion-produit/produit-form/produit-form.component';
import { GestionUtilisateurComponent } from './gestion-utilisateur/gestion-utilisateur.component';
import { UtilisateurFormComponent } from './gestion-utilisateur/utilisateur-form/utilisateur-form.component';
import { GestionCompteComponent } from './gestion-compte/gestion-compte.component';
import { CompteFormComponent } from './gestion-compte/compte-form/compte-form.component';
import { MouvementComponent } from './mouvement/mouvement.component';
import { GestionPackComponent } from './gestion-pack/gestion-pack.component';
import { PackFormComponent } from './gestion-pack/pack-form/pack-form.component';
import { UtilisationProduitsComponent } from './utilisation-produits/utilisation-produits.component';
import { MethodesPaiementComponent } from './methodes-paiement/methodes-paiement.component';
import { TypesOperationsComponent } from './types-operations/types-operations.component';
import { EtudeClientComponent } from './etude-client/etude-client.component';
import { MailComponent } from './mail/mail.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'accueil', component: AccueilComponent },

  { path: 'gestionClient', component: GestionClientComponent },
  { path: 'gestionClient/add', component: ClientFormComponent, data: { action: "add" } },
  { path: 'gestionClient/update/:id', component: ClientFormComponent, data: { action: "update" } },
  { path: 'gestionClient/ClientProduct/:id', component: GestionProduitComponent},

  { path: 'gestionProduit', component: GestionProduitComponent},
  { path: 'gestionProduit/add', component: ProduitFormComponent, data: { action: "add" } },
  { path: 'gestionProduit/update/:id', component: ProduitFormComponent, data: { action: "update" }}, 

 { path: 'gestionUtilisateur', component: GestionUtilisateurComponent},
 { path: 'gestionUtilisateur/add', component: UtilisateurFormComponent, data: { action: "add" } },
 { path: 'gestionUtilisateur/update/:id', component: UtilisateurFormComponent, data: { action: "update" }},

 { path: 'gestionCompte', component: GestionCompteComponent},
 { path: 'gestionCompte/add', component: CompteFormComponent, data: { action: "add" } },
 { path: 'gestionCompte/update/:codeCompte', component: CompteFormComponent, data: { action: "update" }},
 { path: 'gestionClient/gestionCompte/:id', component: GestionCompteComponent},
 { path: 'gestionCompte/gestionMouvement/:id', component: MouvementComponent},
 { path: 'utilisationProduit', component: UtilisationProduitsComponent},
 { path: 'MethodesDePaiement' , component: MethodesPaiementComponent},
 { path: 'TypesOperations' , component: TypesOperationsComponent},
 { path: 'EtudeClients' , component: EtudeClientComponent},
 { path: 'gestionPack', component: GestionPackComponent},
 { path: 'gestionPack/add', component: PackFormComponent, data: { action: "add" } },
 { path: 'gestionPack/update/:id', component: PackFormComponent, data: { action: "update" }},
 { path: 'gestionPack/PackProduct/:id', component: GestionProduitComponent},
 { path: 'mail', component: MailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
