import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
 
  isAdmin:Boolean=false;
  constructor(public sharedService: SharedService,private router: Router) { }
 
  ngOnInit() {
  
      this.isAdmin= this.sharedService.isAdmin();
      if (this.sharedService.getToken() == null) {
        this.router.navigateByUrl('/login');
      }
     
    
  }

}
