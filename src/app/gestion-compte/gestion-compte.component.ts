import { Component, OnInit, Input } from '@angular/core';
import { Compte } from '../models/compte';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { CompteService } from '../services/compte-service';
import { ClientService } from '../services/client-service';
import { Client } from '../models/client';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-gestion-compte',
  templateUrl: './gestion-compte.component.html',
  styleUrls: ['./gestion-compte.component.css']
})
export class GestionCompteComponent implements OnInit {

  @Input() compte: Compte;
  
  constructor(private sharedService: SharedService ,private clientService: ClientService,private compteService: CompteService, private route: ActivatedRoute , private router: Router, private alert: AlertService) { }
  type: string;
  message: string;
  classCss: string;
  eventFire: boolean = true;
  clientId:any=null;
  client:Client;
  ngOnInit() {
    this.route.params.subscribe(params =>
      this.clientId = params['id']
     
    );
    
    if (this.sharedService.getToken() == null) {
      this.router.navigateByUrl('/login');
    }
     this.router.routeReuseStrategy.shouldReuseRoute = function () {
       return false;
     };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    });
    if (this.clientId == null){ 
      this.compteService.getAllComptes().subscribe(
        (data) => {
        
          this.comptesList = data;
     
        }
      )
    }
    else
    {
      this.compteService.getAllComptes().subscribe(
        (data) => {
        
        data.forEach(obj => {
        Object.entries(obj).forEach(([key, value]) => {
          if (key=='client'){
              this.client=value;
              if(this.client.id==this.clientId){
                this.comptesList.push(obj);
              }
          }
           
        });
     
    });
     
        }
      )
    }
    
   
    this.message = this.alert.getMessage();
    this.type = this.alert.getType();
    if (this.message && this.type) {
      this.classCss = this.alert.getClassCss();
    }
  }
  comptesList: Array<Compte> = [];

  ngOnDestroy() {
    if (this.eventFire) {
      this.alert.setAlert(null, null, "d-none stop");
    }
  }

  remove(id: number) {
     this.compteService.deleteCompte(id).subscribe(
       (data) => {
        this.alert.setAlert("success", "Suppression éffectuée !", "d-block play");
         this.eventFire = false;
       
          this.router.navigateByUrl('/gestionCompte');
       },
       (err) => {
        this.alert.setAlert("danger", "Erreur !", "d-block play");
        this.eventFire = false;
         this.router.navigateByUrl('/gestionCompte');
       }
     );
  }


}
