import { Component, OnInit } from '@angular/core';
import { Compte } from 'src/app/models/compte';
import { CompteService } from 'src/app/services/compte-service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-compte-form',
  templateUrl: './compte-form.component.html',
  styleUrls: ['./compte-form.component.css']
})
export class CompteFormComponent implements OnInit {


    compte: Compte = new Compte();
    compteId: any;
    action: string;
    isAjouter:Boolean=false;
    constructor(private compteService: CompteService, private route: ActivatedRoute, private router: Router, public alert: AlertService) { }
  
    ngOnInit() {
     
      this.route.params.subscribe(params =>
        this.compteId = params['codeCompte']
      );
      this.route.data.subscribe(data =>
        this.action = (data.action == "update") ? "Modifier" : "Ajouter" 
        
      )
      this.compteService.getCompteById(this.compteId).subscribe(
        (data: Compte) => this.compte = data
        
      );
      this.isAjouter= (this.action=="Ajouter");
    }
    submit(form: NgForm) {
  
      if (this.action == "Ajouter")
        { this.isAjouter=true;
         
          this.save();}
      else if (this.action == "Modifier")
        this.update(this.compteId);
    }
  
    save() {
      
      this.compteService.createCompte(this.compte)
        .subscribe(data => {
          this.alert.setAlert("success","Compte ajouté !","d-block play")
          this.router.navigateByUrl('/gestionCompte')
  
        }, error => {
          this.alert.setAlert("danger","Erreur ! ajout non éffectué !","d-block play")
          this.router.navigateByUrl('/gestionCompte')
        });
    }
    update(id: any) {
      this.compteService.updateCompte(id, this.compte)
        .subscribe(
          data => {
           
            this.compte = data as Compte;
            this.alert.setAlert("success","Modification éffectuée !","d-block play")
            this.router.navigateByUrl('/gestionCompte');
  
          },
          error => {
            this.alert.setAlert("danger","Erreur ! modification non éffectuée !","d-block play")
            this.router.navigateByUrl('/gestionCompte');
          })
    }
  }
