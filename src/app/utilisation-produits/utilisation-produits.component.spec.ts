import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilisationProduitsComponent } from './utilisation-produits.component';

describe('UtilisationProduitsComponent', () => {
  let component: UtilisationProduitsComponent;
  let fixture: ComponentFixture<UtilisationProduitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilisationProduitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilisationProduitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
