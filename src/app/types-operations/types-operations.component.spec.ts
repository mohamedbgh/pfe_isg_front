import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesOperationsComponent } from './types-operations.component';

describe('TypesOperationsComponent', () => {
  let component: TypesOperationsComponent;
  let fixture: ComponentFixture<TypesOperationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypesOperationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
