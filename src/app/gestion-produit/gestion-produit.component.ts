import { Component, OnInit, Input } from '@angular/core';
import { Produit } from '../models/produit';
import { ProduitService } from '../services/produit-service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { SharedService } from '../services/shared.service';
import { Client } from '../models/client';
import { ClientService } from '../services/client-service';



@Component({
  selector: 'app-gestion-produit',
  templateUrl: './gestion-produit.component.html',
  styleUrls: ['./gestion-produit.component.css']
})
export class GestionProduitComponent implements OnInit {

  @Input() produit:Produit;
  constructor(private sharedService: SharedService ,private clientService: ClientService ,private produitService: ProduitService,private route: ActivatedRoute , private router: Router, private alert: AlertService) { }
  type: string;
  message: string;
  classCss: string;
  eventFire: boolean = true;
  clientId : any=null;
  Client : Client
  
 
  produitsList: any;
 
  ngOnInit() {
    this.route.params.subscribe(params =>
      this.clientId = params['id']
     
    );
  
    if (this.sharedService.getToken() == null) {
      this.router.navigateByUrl('/login');
    }
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

   this.router.events.subscribe((evt) => {
     if (evt instanceof NavigationEnd) {
       this.router.navigated = false;
       window.scrollTo(0, 0);
     }
   });
   
   if(this.clientId== null){

    this.produitService.getAllProduits().subscribe(
      (data) => {
    
        this.produitsList = data;
       
      }
    )
  }
    
    if (this.clientId !=null){
      
       this.clientService.getProduitForClient(this.clientId).subscribe(
        (data) => {
      
          this.produitsList = data;
     //     console.log(this.produitsListClient)
         
        }
      )
       
      }
    
 
   this.message = this.alert.getMessage();
   this.type = this.alert.getType();
   if (this.message && this.type) {
     this.classCss = this.alert.getClassCss();
   }
 }
 

  





 ngOnDestroy() {
   if (this.eventFire) {
     this.alert.setAlert(null, null, "d-none stop");
   }
 }

 remove(id: number) {
    this.produitService.deleteProduit(id).subscribe(
      (data) => {
       this.alert.setAlert("success", "Suppression éffectuée !", "d-block play");
        this.eventFire = false;
        this.router.navigateByUrl('/gestionProduit');
      },
      (err) => {
       this.alert.setAlert("danger", "Erreur !", "d-block play");
       this.eventFire = false;
       this.router.navigateByUrl('/gestionProduit');
      }
    );
 }

  }

