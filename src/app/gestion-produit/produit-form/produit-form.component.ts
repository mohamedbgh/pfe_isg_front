import { Component, OnInit } from '@angular/core';
import { Produit } from 'src/app/models/produit';
import { ActivatedRoute, Router } from '@angular/router';
import { ProduitService } from 'src/app/services/produit-service';
import { AlertService } from 'src/app/services/alert.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-produit-form',
  templateUrl: './produit-form.component.html',
  styleUrls: ['./produit-form.component.css']
})
export class ProduitFormComponent implements OnInit {

  produit: Produit = new Produit();
  produitId: any;
  action: string;
  isAjouter:Boolean=false;
  constructor(private produitService: ProduitService, private route: ActivatedRoute, private router: Router, public alert: AlertService) { }
  

  ngOnInit() {
    this.route.params.subscribe(params =>
      this.produitId = params['id']
    );
    this.route.data.subscribe(data =>
      this.action = (data.action == "update") ? "Modifier" : "Ajouter"
    )
    this.produitService.getProduitById(this.produitId).subscribe(
      (data: Produit) => this.produit = data
      
    );
    this.isAjouter= (this.action=="Ajouter");
    
  }

submit(form: NgForm) {
  
    if (this.action == "Ajouter")
      this.save();
    else if (this.action == "Modifier")
      this.update(this.produitId);
  }

  save() {
    
    this.produitService.createProduit(this.produit)
      .subscribe(data => {
        this.alert.setAlert("success","Produit ajouté !","d-block play")
        this.router.navigateByUrl('/gestionProduit')

      }, error => {
        this.alert.setAlert("danger","Erreur ! ajout non éffectué !","d-block play")
        this.router.navigateByUrl('/gestionProduit')
      });
  }
  update(id: any) {
    this.produitService.updateProduit(id, this.produit)
      .subscribe(
        data => {
         
          this.produit = data as Produit;
          this.alert.setAlert("success","Modification éffectuée !","d-block play")
          this.router.navigateByUrl('/gestionProduit');

        },
        error => {
          this.alert.setAlert("danger","Erreur ! modification non éffectuée !","d-block play")
          this.router.navigateByUrl('/gestionProduit');
        })
  }
}
