import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { AccueilComponent } from './accueil/accueil.component';
import { GestionClientComponent } from './gestion-client/gestion-client.component'
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NavbarComponent } from './navbar/navbar.component';
import { ClientFormComponent } from './gestion-client/client-form/client-form.component';
import { GestionProduitComponent } from './gestion-produit/gestion-produit.component';
import { ProduitFormComponent } from './gestion-produit/produit-form/produit-form.component';
import { GestionUtilisateurComponent } from './gestion-utilisateur/gestion-utilisateur.component';
import { UtilisateurFormComponent } from './gestion-utilisateur/utilisateur-form/utilisateur-form.component';
import { GestionCompteComponent } from './gestion-compte/gestion-compte.component';
import { CompteFormComponent } from './gestion-compte/compte-form/compte-form.component';
import { MouvementComponent } from './mouvement/mouvement.component';
import { GestionPackComponent } from './gestion-pack/gestion-pack.component';
import { PackFormComponent } from './gestion-pack/pack-form/pack-form.component';
import{UtilisationProduitsComponent} from './utilisation-produits/utilisation-produits.component';
import { MethodesPaiementComponent } from './methodes-paiement/methodes-paiement.component';
import { TypesOperationsComponent } from './types-operations/types-operations.component';
import { EtudeClientComponent } from './etude-client/etude-client.component';
import { MailComponent } from './mail/mail.component'





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccueilComponent,
    GestionClientComponent,
    NavbarComponent,
    ClientFormComponent,
    GestionProduitComponent,
    ProduitFormComponent,
    GestionUtilisateurComponent,
    UtilisateurFormComponent,
    GestionCompteComponent,
    CompteFormComponent,
    MouvementComponent,
    GestionPackComponent,
    PackFormComponent,
    UtilisationProduitsComponent,
    MethodesPaiementComponent,
    TypesOperationsComponent,
    EtudeClientComponent,
    MailComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, HttpClientModule, FormsModule, MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
