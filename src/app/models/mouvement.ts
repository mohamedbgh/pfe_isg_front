  export enum Nature{
    DEBIT ,
    CREDIT
}

export enum Sens{
    Carte,
    Chéque, 
    Espéce
}


export class Mouvement {
    numero ?: number;
    nature ?: Nature;
    montant ?:number;
    dateOperation ?: Date;
    sens ?:Sens;
   
}


