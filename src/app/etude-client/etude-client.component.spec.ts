import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudeClientComponent } from './etude-client.component';

describe('EtudeClientComponent', () => {
  let component: EtudeClientComponent;
  let fixture: ComponentFixture<EtudeClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtudeClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudeClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
