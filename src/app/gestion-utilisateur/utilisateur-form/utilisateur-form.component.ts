import { Component, OnInit } from '@angular/core';
import { utilisateur } from 'src/app/models/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur-service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { NgForm } from '@angular/forms';
import Swal  from 'sweetalert2';


@Component({
  selector: 'app-utilisateur-form',
  templateUrl: './utilisateur-form.component.html',
  styleUrls: ['./utilisateur-form.component.css']
})
export class UtilisateurFormComponent implements OnInit {

  user: utilisateur = new utilisateur();
  userId: any;
  action: string;
  
  
  
  constructor(private utilisateurService: UtilisateurService, private route: ActivatedRoute, private router: Router, public alert: AlertService) { }

  ngOnInit() {
    this.route.params.subscribe(params =>
      this.userId = params['id']
    );
    this.route.data.subscribe(data =>
      this.action = (data.action == "update") ? "Modifier" : "Ajouter"
    )
    this.utilisateurService.getUserById(this.userId).subscribe(
      (data: utilisateur) => this.user = data
      
    );
    
  }
  submit(form: NgForm) {
  
    if (this.action == "Ajouter")
      this.save();
    else if (this.action == "Modifier")
      this.update(this.userId);
  }

  save() {
    
    this.utilisateurService.createUser(this.user)
      .subscribe(data => {
        Swal.fire(
          'Utilisateur ajouté avec succès',
          '',
          'success',  
        )
        this.router.navigateByUrl('/gestionUtilisateur')

      }, error => {
     
        this.router.navigateByUrl('/gestionUtilisateur')
      });
  }
  update(id: any) {
    this.utilisateurService.updateUser(id, this.user)
      .subscribe(
        data => {
         
          this.user = data as utilisateur;
          Swal.fire(
            'Modification Effectuée avec succès',
            '',
            'success',  
          )
          this.router.navigateByUrl('/gestionUtilisateur');

        },
        error => {
          this.alert.setAlert("danger","Erreur ! modification non éffectuée !","d-block play")
          this.router.navigateByUrl('/gestionUtilisateur');
        })
  }


}
