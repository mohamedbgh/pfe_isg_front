import { Component, OnInit, Input } from '@angular/core';
import { utilisateur } from '../models/utilisateur';
import { UtilisateurService } from '../services/utilisateur-service';
import { Router, NavigationEnd } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { SharedService } from '../services/shared.service';
import Swal  from 'sweetalert2';
@Component({
  selector: 'app-gestion-utilisateur',
  templateUrl: './gestion-utilisateur.component.html',
  styleUrls: ['./gestion-utilisateur.component.css']
})
export class GestionUtilisateurComponent implements OnInit {

  @Input() user: utilisateur;
  constructor(private sharedService: SharedService ,private utilisateurService: UtilisateurService,  private router: Router, private alert: AlertService) { }
  type: string;
  message: string;
  classCss: string;
  eventFire: boolean = true;
  ngOnInit() {
    if (this.sharedService.getToken() == null) {
      this.router.navigateByUrl('/login');
    }
     this.router.routeReuseStrategy.shouldReuseRoute = function () {
       return false;
     };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    });
    this.utilisateurService.getAllUsers().subscribe(
      (data) => {
        this.usersList = data;
    
      }
    )
  console.log(this.alert)
    this.message = this.alert.getMessage();
    this.type = this.alert.getType();
    if (this.message && this.type) {
      this.classCss = this.alert.getClassCss();
    }
  }
  usersList: Array<utilisateur> = [];

  ngOnDestroy() {
    if (this.eventFire) {
      this.alert.setAlert(null, null, "d-none stop");
    }
  }

  remove(id: number) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

       
     

        this.utilisateurService.deleteUser(id).subscribe(
          (data) => {
          
            this.eventFire = false;
            this.router.navigateByUrl('/gestionUtilisateur');
          },
          (err) => {
           this.eventFire = false;
           this.router.navigateByUrl('/gestionUtilisateur');
          }
        );



        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        }
    })


     
  }

 

}
