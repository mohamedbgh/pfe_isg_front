import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Pack } from '../models/pack';




@Injectable({
    providedIn: 'root'
  })
  export class PackService{
    private baseUrl = "http://localhost:8080" + '/Packs';
    private baseUrl2 = "http://localhost:8080" + '/Pack';
    private baseUrl3 = "http://localhost:8080" + '/PackProduct';

    constructor(private http: HttpClient){

    }
    getPack(login: any): Observable<Object> {
       
        return this.http.get(`${this.baseUrl}/${login}`);
        
    }
    getPackById(id: any): Observable<Object> {
       
      return this.http.get(`${this.baseUrl2}/${id}`);
      
  }
    getAllPacks(): Observable<any> {
        
        return this.http.get(`${this.baseUrl}`);
        
    }
    createPack(pack: Pack): Observable<Object> {
      
        return this.http.post(`${this.baseUrl}`, pack);
      }
    
      updatePack(id: number, value: any): Observable<Object> {
         return this.http.put(`${this.baseUrl}/${id}`, value);
       }
    
      deletePack(id: number): Observable<any> {
       return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
      }
      getProduitFromPack(id: any): Observable<Object> {
        return this.http.get(`${this.baseUrl3}/${id}`);
      }
  }