import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client';


@Injectable({
    providedIn: 'root'
  })
export class ClientService {

    private baseUrl = "http://localhost:8080" + '/Clients';
    private baseUrl2 = "http://localhost:8080" + '/Client';
    private baseUrl3 = "http://localhost:8080" + '/ClientProduct';

    constructor(private http: HttpClient){

    }
    getClient(login: any): Observable<Object> {
       
        return this.http.get(`${this.baseUrl}/${login}`);
        
    }
    getClientById(id: any): Observable<Object> {
       
      return this.http.get(`${this.baseUrl2}/${id}`);
      
  }
    getAllClients(): Observable<any> {
        
        return this.http.get(`${this.baseUrl}`);
        
    }
    createClient(customer: Client): Observable<Object> {
      
        return this.http.post(`${this.baseUrl}`, customer);
      }
    
      updateClient(id: number, value: any): Observable<Object> {
         return this.http.put(`${this.baseUrl}/${id}`, value);
       }
    
      deleteClient(id: number): Observable<any> {
       return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
      }
      getProduitForClient(id: any): Observable<Object> {
        return this.http.get(`${this.baseUrl3}/${id}`);
      }
}

