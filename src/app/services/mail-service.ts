import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mail } from '../models/mail';


@Injectable({
    providedIn: 'root'
  })
export class MailService {
    
   

    constructor(private http: HttpClient){

    }
    sendMail(mail): Observable<Object> {
      
         console.log(mail.subject);
        return this.http.post("http://localhost:8080/send", mail); 
      }

     
    }

