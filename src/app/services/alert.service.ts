import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  type: string = null;
  message: string = null;
  classCss: string = "d-none stop";
  constructor() { }

  setAlert(type, message, classCss) {
    this.setType(type);
    this.setMessage(message);
    this.setClassCss(classCss);
  }

  getType() {
    return this.type;
  }
  setType(type) {
    this.type = type;
  }

  getMessage() {
    return this.message;
  }
  setMessage(message) {
    this.message = message;
  }

  getClassCss() {
    return this.classCss;
  }
  setClassCss(classCss) {
    this.classCss = classCss;
  }
}