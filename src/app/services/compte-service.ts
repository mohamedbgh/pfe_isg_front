import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Compte } from '../models/compte';
import { ClientService } from './client-service';
import { Client } from '../models/client';



@Injectable({
    providedIn: 'root'
  })
export class CompteService {

    private baseUrl = "http://localhost:8080" + '/Comptes';
    private baseUrl2 = "http://localhost:8080" + '/Compte';
    private baseUrl3 = "http://localhost:8080" + '/CompteAdd';
    private baseUrl4 = "http://localhost:8080" + '/CompteUpdate';
    private baseUrl5 = "http://localhost:8080" + '/ComptetDeletebyId';
    client:Client;
    constructor(private http: HttpClient,private clientservice:ClientService){

    }
    getCompte(login: any): Observable<Object> {
       
        return this.http.get(`${this.baseUrl}/${login}`);
        
    }
    getCompteById(codeCompte: any): Observable<Object> {
       
      return this.http.get(`${this.baseUrl2}/${codeCompte}`);
      
  }
    getAllComptes(): Observable<any> {
        
        return this.http.get(`${this.baseUrl}`);
        
    }
    createCompte(customer: Compte): Observable<Object> {
         let codeCompte=customer.client_id;
        return this.http.post(`${this.baseUrl3}/${codeCompte}`, customer);
      }
    
      updateCompte(codeCompte: number, value: any): Observable<Object> {
         return this.http.put(`${this.baseUrl4}/${codeCompte}`, value);
       }
    
      deleteCompte(codeCompte: number): Observable<any> {
       return this.http.delete(`${this.baseUrl5}/${codeCompte}`, { responseType: 'text' });
      }
}