import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  constructor(private http: HttpClient, private sharedService: SharedService) {


  }

  login(user) {
  
    return this.http.post(SharedService.url + "/authenticate", user, { observe: 'response' });
}

logout() {
  this.sharedService.setToken(null);
  this.sharedService.setRole(null);
  localStorage.removeItem('token');
  localStorage.removeItem('role');
}

}
