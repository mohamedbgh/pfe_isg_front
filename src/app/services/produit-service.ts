import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Produit } from '../models/produit';
import { HttpClient} from '@angular/common/http';




@Injectable({
    providedIn: 'root'
  })
export class ProduitService {

    private baseUrl = "http://localhost:8080" + '/Produits';
    private baseUrl2 = "http://localhost:8080" + '/Produit';

    constructor(private http: HttpClient){

    }
    getProduit(login: any): Observable<Object> {
       
        return this.http.get(`${this.baseUrl}/${login}`);
        
    }
    getProduitById(id: any): Observable<Object> {
       
      return this.http.get(`${this.baseUrl2}/${id}`);
      
  }
    getAllProduits(): Observable<any> {
        
        return this.http.get(`${this.baseUrl}`);
        
    }
    createProduit(produit: Produit): Observable<Object> {
      
        return this.http.post(`${this.baseUrl}`, produit);
      }
    
      updateProduit(id: number, value: any): Observable<Object> {
         return this.http.put(`${this.baseUrl}/${id}`, value);
       }
    
      deleteProduit(id: number): Observable<any> {
       return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
      }
}