import { Injectable } from '@angular/core';
import { JwtHelperService } from '../../../node_modules/@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private jwtToken = null;
  private role: string;

  static url: string = "http://localhost:8080";

  constructor() { }

    setToken(token) {
     
      this.jwtToken = token;
      localStorage.setItem('token', token);
      
    }
  
  getToken() {
    if (this.jwtToken == null) this.jwtToken = localStorage.getItem('token');
    return this.jwtToken;
  }

  setRole(role) {
    localStorage.setItem('role', role);
  }
  isAdmin() {
    return localStorage.getItem('role')=="[admin]";
  }

}
