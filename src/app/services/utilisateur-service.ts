import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client';
import { AuthentificationService } from './authentification.service';
import { SharedService } from './shared.service';


@Injectable({
    providedIn: 'root'
  })
export class UtilisateurService {

    private baseUrl = "http://localhost:8080" + '/Users';
    private baseUrl2 = "http://localhost:8080" + '/User';

    constructor(private http: HttpClient, public authService: AuthentificationService, public sharedService: SharedService){

    }
    
    getUser(login: any): Observable<Object> {

        return this.http.get(`${this.baseUrl}/${login}`, { headers: new HttpHeaders({ 'Authorization': this.sharedService.getToken() }) });
        
    }
    getUserById(id: any): Observable<Object> {
       
      return this.http.get(`${this.baseUrl2}/${id}`, { headers: new HttpHeaders({ 'Authorization': this.sharedService.getToken() }) });
      
  }
    getAllUsers(): Observable<any> {
       
        return this.http.get(`${this.baseUrl}`, { headers: new HttpHeaders({ 'Authorization': this.sharedService.getToken() }) });
        
    }
    createUser(customer: Client): Observable<Object> {
      
        return this.http.post(`${this.baseUrl}`, customer , { headers: new HttpHeaders({ 'Authorization': this.sharedService.getToken() }) });
      }
    
      updateUser(id: number, value: any): Observable<Object> {
         return this.http.put(`${this.baseUrl}/${id}`, value, { headers: new HttpHeaders({ 'Authorization': this.sharedService.getToken() }) });
       }
    
      deleteUser(id: number): Observable<any> {
       return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' ,headers: new HttpHeaders({ 'Authorization': this.sharedService.getToken() }) });
      }
}

