import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MouvementService {

  constructor(private http: HttpClient) { }
  private baseUrl = "http://localhost:8080" + '/Mouvements';
  getAllMouvements(): Observable<any> {
        
    return this.http.get(`${this.baseUrl}`);
    
}
}
