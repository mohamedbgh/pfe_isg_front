import { Component, OnInit, Input } from '@angular/core';
import { Client } from '../models/client';
import { ClientService } from '../services/client-service';
import { AlertService } from '../services/alert.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-gestion-client',
  templateUrl: './gestion-client.component.html',
  styleUrls: ['./gestion-client.component.css']
})
export class GestionClientComponent implements OnInit {


@Input() client: Client;
  constructor(private sharedService:SharedService ,private clientService: ClientService,  private router: Router, private alert: AlertService) { }
  type: string;
  message: string;
  classCss: string;
  eventFire: boolean = true;
  ngOnInit() {
    if (this.sharedService.getToken() == null) {
      this.router.navigateByUrl('/login');
    }
     this.router.routeReuseStrategy.shouldReuseRoute = function () {
       return false;

     };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    });
    this.clientService.getAllClients().subscribe(
      (data) => {
       
        this.clientsList = data;
   
      }
    )
  
    this.message = this.alert.getMessage();
    this.type = this.alert.getType();
    if (this.message && this.type) {
      this.classCss = this.alert.getClassCss();
    }
  }
  clientsList: Array<Client> = [];

  ngOnDestroy() {
    if (this.eventFire) {
      this.alert.setAlert(null, null, "d-none stop");
    }
  }

  remove(id: number) {
     this.clientService.deleteClient(id).subscribe(
       (data) => {
        this.alert.setAlert("success", "Suppression éffectuée !", "d-block play");
         this.eventFire = false;
         this.router.navigateByUrl('/gestionClient');
       },
       (err) => {
        this.alert.setAlert("danger", "Erreur !", "d-block play");
        this.eventFire = false;
        this.router.navigateByUrl('/gestionClient');
       }
     );
  }

 
}
