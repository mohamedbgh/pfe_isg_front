import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/client';
import { ClientService } from 'src/app/services/client-service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { NgForm } from '@angular/forms';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {

  client: Client = new Client();
  clientId: any;
  action: string;
  constructor(private sharedService: SharedService ,private clientService: ClientService, private route: ActivatedRoute, private router: Router, public alert: AlertService) { }

  ngOnInit() {
    this.route.params.subscribe(params =>
      this.clientId = params['id']
    );
    this.route.data.subscribe(data =>
      this.action = (data.action == "update") ? "Modifier" : "Ajouter"
    )
    this.clientService.getClientById(this.clientId).subscribe(
      (data: Client) => this.client = data
      
    );
    
  }
  submit(form: NgForm) {
   
    if (this.action == "Ajouter")
      this.save();
    else if (this.action == "Modifier")
      this.update(this.clientId);
     
  }

  save() {
    
    this.clientService.createClient(this.client)
      .subscribe(data => {
        this.alert.setAlert("success","Client ajouté !","d-block play")
        this.router.navigateByUrl('/gestionClient')

      }, error => {
        this.alert.setAlert("danger","Erreur ! ajout non éffectué !","d-block play")
        this.router.navigateByUrl('/gestionClient')
      });
  }
  update(id: any) {
    this.clientService.updateClient(id, this.client)
      .subscribe(
        data => {
         
          this.client = data as Client;
          this.alert.setAlert("success","Modification éffectuée !","d-block play")
          this.router.navigateByUrl('/gestionClient');

        },
        error => {
          this.alert.setAlert("danger","Erreur ! modification non éffectuée !","d-block play")
          this.router.navigateByUrl('/gestionClient');
        })
  }

}
